public class Article {

    private String type;
    private String id;
    private String highway;
    private String ref;
    private String surface;
    private String lanes;
    private String maxSpeed;
    private String isOneWay;
    private String toll;

    public Article() {

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHighway() {
        return highway;
    }

    public void setHighway(String highway) {
        this.highway = highway;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getSurface() {
        return surface;
    }

    public void setSurface(String surface) {
        this.surface = surface;
    }

    public String getLanes() {
        return lanes;
    }

    public void setLanes(String lanes) {
        this.lanes = lanes;
    }

    public String getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(String maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public String getIsOneWay() {
        return isOneWay;
    }

    public void setIsOneWay(String isOneWay) {
        this.isOneWay = isOneWay;
    }

    public String getToll() {
        return toll;
    }

    public void setToll(String toll) {
        this.toll = toll;
    }

    @Override
    public String toString() {
        return "Article{" +
                "type='" + type + '\'' +
                ", id='" + id + '\'' +
                ", highway='" + highway + '\'' +
                ", ref='" + ref + '\'' +
                ", surface='" + surface + '\'' +
                ", lanes='" + lanes + '\'' +
                ", maxSpeed='" + maxSpeed + '\'' +
                ", isOneWay='" + isOneWay + '\'' +
                ", toll='" + toll + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Article article = (Article) o;

        return getRef().equals(article.getRef());
    }

    @Override
    public int hashCode() {
        return getRef().hashCode();
    }
}
