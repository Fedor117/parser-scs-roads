import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by fedor on 25.11.16.
 */
public class Utils {

    public static final String DIR_NAME = "generated_articles";
    public static final String FILE_NAME = "data.geojson";

    /* Don't touch */
    public static void saveArticle(Article article) {
        StringBuilder builder = new StringBuilder(1024);

        builder.append("sc_node_not_relation -> concept_highway_")
                .append(article.getRef())
                .append(";;\n\n");

        builder.append("concept_highway_")
                .append(article.getRef())
                .append(" <- concept_republican_road;;\n\n");

        builder.append("concept_highway_")
                .append(article.getRef())
                .append(" <- concept_terrain_object;;\n\n");

        builder.append("concept_highway_")
                .append(article.getRef())
                .append(" => nrel_main_idtf:\n")
                .append("\t[")
                .append("дорога " + article.getRef())
                .append("]\n")
                .append("\t(* <- lang_ru;; *);\n")
                .append("\t[")
                .append("highway " + article.getRef())
                .append("]\n")
                .append("\t(* <- lang_en;; *);;\n\n");

        if (!article.getId().equals("null")) {
            builder.append("concept_highway_")
                    .append(article.getRef())
                    .append(" <= nrel_sc_text_translation:\n")
                    .append("\t\t...\n")
                    .append("\t\t(*\n")
                    .append("\t\t-> rrel_example:\n")
                    .append("\t\t\t[")
                    .append(article.getId())
                    .append("]\n")
                    .append("\t\t\t(* <- lang_en;; *);;\n")
                    .append("\t\t*);;\n\n");
        }

        if (!article.getHighway().equals("null")) {
            builder.append("concept_highway_")
                    .append(article.getRef())
                    .append(" => nrel_highway_type:\n")
                    .append("\tconcept_")
                    .append(article.getHighway())
                    .append(";;\n\n")

                    .append("concept_highway_")
                    .append(article.getRef())
                    .append(" => nrel_osm_query:\n")
                    .append("\t[way[\"highway\"=\"")
                    .append(article.getHighway())
                    .append("\"][\"ref\"=\"")
                    .append(enCharToRu(article.getRef()))
                    .append("\"];];;\n\n");
        }

        if (!article.getMaxSpeed().equals("null")) {
            builder.append("concept_highway_")
                    .append(article.getRef())
                    .append(" => nrel_max_speed:\n")
                    .append("\t[")
                    .append(article.getMaxSpeed())
                    .append("]")
                    .append(";;\n\n");
        }

        if (!article.getLanes().equals("null")) {
            builder.append("concept_highway_")
                    .append(article.getRef())
                    .append(" => nrel_lanes_num:\n")
                    .append("\t[")
                    .append(article.getLanes())
                    .append("]")
                    .append(";;\n\n");
        }

        if (!article.getSurface().equals("null")) {
            builder.append("concept_highway_")
                    .append(article.getRef())
                    .append(" => nrel_surface:\n")
                    .append("\tconcept_")
                    .append(article.getSurface())
                    .append(";;\n\n");
        }

        if (!article.getIsOneWay().equals("null")) {
            if (article.getIsOneWay().equals("yes")) {
                builder.append("concept_highway_")
                        .append(article.getRef())
                        .append(" <- concept_one_way;;\n\n");
            } else {
                builder.append("concept_highway_")
                        .append(article.getRef())
                        .append(" <- concept_multiple_way;;\n\n");
            }
        }

        if (!article.getToll().equals("null")) {
            if (article.getToll().equals("yes")) {
                builder.append("concept_highway_")
                        .append(article.getRef())
                        .append(" <- concept_toll_road;;\n\n");
            } else {
                builder.append("concept_highway_")
                        .append(article.getRef())
                        .append(" <- concept_free_road;;\n\n");
            }
        }

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(
                    new File(DIR_NAME + "/" +
                            "concept_highway_" + article.getRef() + ".scs")));
            writer.write(builder.toString());
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String readFile(String fileName) {
        StringBuilder result = new StringBuilder("");
        ClassLoader classLoader = Utils.class.getClassLoader();

        File file = null;
        try {
             file = new File(classLoader.getResource(fileName).getFile());
        } catch (NullPointerException e) {
            System.out.println("Problem with reading the file. See you soon!");
            e.printStackTrace();
            System.exit(1);
        }

        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                result.append(line);
                result.append(System.lineSeparator());
            }
            scanner.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result.toString();
    }

    public static String ruCharToEn(String str) {
        String temp = String.valueOf(str.charAt(0));
        StringBuilder builder = new StringBuilder(str);
        if (temp.equals("М") || temp.equals("м")) {
            builder.setCharAt(0, 'M');
        } else if (temp.equals("Р") || temp.equals("р")) {
            builder.setCharAt(0, 'P');
        }
        return builder.toString();
    }

    public static String enCharToRu(String str) {
        String temp = String.valueOf(str.charAt(0));
        StringBuilder builder = new StringBuilder(str);
        if (temp.equals("M") || temp.equals("m")) {
            builder.setCharAt(0, 'М');
        } else if (temp.equals("P") || temp.equals("p")) {
            builder.setCharAt(0, 'Р');
        }
        return builder.toString();
    }

}
