import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.Set;

public class JsonParser {

    public void parse(String jsonStr) {
        final String FEATURES = "features";
        final String PROPERTIES = "properties";
        final String TYPE = "type";
        final String ID = "@id";
        final String HIGHWAY = "highway";
        final String REF = "ref";
        final String SURFACE = "surface";
        final String LANES = "lanes";
        final String MAXSPEED = "maxspeed";
        final String ONEWAY = "oneway";
        final String TOLL = "toll";

        Set<Article> articles = new HashSet<>(); // Doesn't allow duplicates

        try {
            JSONObject geoJson = new JSONObject(jsonStr);
            JSONArray geoArray = geoJson.getJSONArray(FEATURES);

            for (int i = 0; i < geoArray.length(); i++) {
                JSONObject roadObject = geoArray.getJSONObject(i);
                JSONObject prop = roadObject.getJSONObject(PROPERTIES);

                Article article = new Article();
                article.setType(checkAttr(prop, TYPE));
                article.setId(checkAttr(prop, ID));
                article.setHighway(checkAttr(prop, HIGHWAY));
                article.setRef(Utils.ruCharToEn(checkAttr(prop, REF)));
                article.setSurface(checkAttr(prop, SURFACE));
                article.setLanes(checkAttr(prop, LANES));
                article.setMaxSpeed(checkAttr(prop, MAXSPEED));
                article.setIsOneWay(checkAttr(prop, ONEWAY));
                article.setToll(checkAttr(prop, TOLL));

                System.out.println(article); // For test purpose only
                articles.add(article);
            }

        } catch (JSONException e) {
            System.out.println("No JSON Objects left. See you soon!");
        }

        System.out.println(articles.size());

        for (Article article : articles) {
            Utils.saveArticle(article);
        }
    }

    private String checkAttr(JSONObject object, String attr) {
        return object.has(attr) ? object.getString(attr) : "null";
    }

}
