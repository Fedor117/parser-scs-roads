/**
 * Created by fedor on 25.11.16.
 */
public class EntryPoint {

    public static void main(String[] args) {
        JsonParser parser = new JsonParser();
        parser.parse(Utils.readFile(Utils.FILE_NAME));
    }

}
